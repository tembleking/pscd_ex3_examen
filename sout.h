#ifndef SOUT_H
#define SOUT_H

#include <mutex>
#include <ostream>


// Synchronized output to a standard stream
class sout {
    static std::recursive_mutex mtx;
    std::ostream *ostream;
    std::unique_lock<std::recursive_mutex> lck;

public:
    sout(std::ostream *ostream) : ostream(ostream), lck(sout::mtx) {}

    template<typename T>
    sout &operator<<(T value) {
        *ostream << value;
        return *this;
    }

    sout &operator<<(std::ostream &(*func)(std::ostream &)) {
        *ostream << func;
        return *this;
    }
};

std::recursive_mutex sout::mtx;
#endif //SOUT_H
