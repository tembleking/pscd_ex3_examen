#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <condition_variable>
#include <queue>
#include "sout.h"

class WashingTunnel {
    std::mutex mtx;
    std::condition_variable cv;
    bool enabled;
    int carId;

public:

    void PutCar(int coche) {
        std::unique_lock<std::mutex> lck(mtx);
        enabled = true;
        carId = coche;
        cv.notify_all();
    }

    void PopCar() {
        std::unique_lock<std::mutex> lck(mtx);
        enabled = false;
        cv.notify_all();
    }

    int GetCurrentCar() {
        std::unique_lock<std::mutex> lck(mtx);
        cv.wait(lck, [this]() { return enabled; });
        return carId;
    }

    void WaitUntilFinished() {
        std::unique_lock<std::mutex> lck(mtx);
        cv.wait(lck, [this]() { return !enabled; });
    }
};

class Parking {
    typedef std::shared_ptr<std::condition_variable> cvPtr;
    std::condition_variable cvCars;
    std::mutex mtx;
    std::queue<cvPtr> queue;
    std::queue<int> queueCarId;
public:

    void ParkCarById(int id) {
        std::unique_lock<std::mutex> lck(mtx);
        cvPtr cv = queue.emplace(std::make_shared<std::condition_variable>());
        queueCarId.emplace(id);
        cvCars.notify_all();
        if (cv) cv->wait(lck);
    }

    int GetNextCar() {
        std::unique_lock<std::mutex> lck(mtx);
        cvCars.wait(lck, [this]() { return !queueCarId.empty(); });
        auto nextCarId = queueCarId.front();
        queueCarId.pop();
        return nextCarId;
    }

    void NotifyOwner() {
        std::unique_lock<std::mutex> lck(mtx);
        if (queue.empty())
            return;

        cvPtr cv = queue.front();
        queue.pop();
        cv->notify_all();
    }
};

class Employee {
    std::thread thread;
    bool running;
    std::shared_ptr<WashingTunnel> washingTunnel;
    std::shared_ptr<Parking> parking;

    static void run(Employee *self) {
        while (self->running) {
            // Gets the next car if any, or blocks until one arrives
            auto nextCar = self->parking->GetNextCar();
            sout(&std::cout) << "I'm cleaning the car with ID: " << nextCar << std::endl;

            // Gets the car and introduces it in the washing tunnel
            self->washingTunnel->PutCar(nextCar);

            // Enables the washing process and waits
            sout(&std::cout) << "I'll wait until it has finished" << std::endl;
            self->washingTunnel->WaitUntilFinished();

            // When it has finished, notifies the owner
            sout(&std::cout) << "I'll just notify the owner of the car with ID: " << nextCar << std::endl;
            self->parking->NotifyOwner();
        }
    }

public:
    explicit Employee(std::shared_ptr<Parking> parking, std::shared_ptr<WashingTunnel> tunnel) : parking(
            std::move(parking)), washingTunnel(std::move(tunnel)), running(true) {
        thread = std::thread(Employee::run, this);
    }

    virtual ~Employee() {
        running = false;
        if (thread.joinable())
            thread.join();
    }
};


class TunnelControl {
    std::thread thread;
    bool running;
    std::shared_ptr<WashingTunnel> washingTunnel;

    static void run(TunnelControl *self) {
        while (self->running) {
            // When its enabled, wash the car
            auto carId = self->washingTunnel->GetCurrentCar();
            sout(&std::cout) << "Washing the car with ID: " << carId << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(2));

            // Finished washing the car
            self->washingTunnel->PopCar();
        }
    }

public:
    explicit TunnelControl(std::shared_ptr<WashingTunnel> washingTunnel) : washingTunnel(std::move(washingTunnel)),
                                                                         running(true) {
        thread = std::thread(TunnelControl::run, this);
    }

    virtual ~TunnelControl() {
        running = false;
        if (thread.joinable())
            thread.join();
    }
};


class User {
    static int globalId;
    std::thread thread;
    int id;
    bool running;
    std::shared_ptr<Parking> parking;

    static void run(User *self) {
        while (self->running) {
            sout(&std::cout) << "I'm going to park my car, with ID: " << self->id << std::endl;
            self->parking->ParkCarById(self->id);
            sout(&std::cout) << "I got my car cleaned with ID: " << self->id << std::endl;
        }
    }

public:
    explicit User(std::shared_ptr<Parking> parking) : parking(std::move(parking)), running(true), id(globalId++) {
        thread = std::thread(User::run, this);
    }

    virtual ~User() {
        running = false;
        if (thread.joinable())
            thread.join();
    }

};

int User::globalId = 0;

int main() {
    auto parking = std::make_shared<Parking>();
    auto tunnel = std::make_shared<WashingTunnel>();

    Employee employee(parking, tunnel);
    TunnelControl control(tunnel);

    std::vector<std::shared_ptr<User>> users;
    for (int i = 0; i < 100; ++i) {
        users.emplace_back(std::make_shared<User>(parking));
    }

    std::this_thread::sleep_for(std::chrono::minutes(5));
}